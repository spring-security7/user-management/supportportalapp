import {
  HttpErrorResponse,
  HttpEvent,
  HttpEventType,
} from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { BehaviorSubject, Subscription } from 'rxjs';
import { SubSink } from 'subsink';
import { NotificationType } from '../enums/notification-type';
import { Role } from '../enums/role';
import { CustomHttpResponse } from '../models/custom-http-response';
import { FileUploadStatus } from '../models/file-upload.status';
import { User } from '../models/user';
import { AuthenticationService } from '../services/authentication.service';
import { NotificationService } from '../services/notification.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css'],
})
export class UserComponent implements OnInit, OnDestroy {
  //subSink: library to unsubscribe from observables
  private subs = new SubSink();

  private titleSubject = new BehaviorSubject<string>('Users');
  //with edit: if user changes it, and this is not done, it will fail in he backend
  private currentUserName: string;
  private subscriptions: Subscription[] = [];

  //titleAction$ ==> a listener for event as observable
  public titleAction$ = this.titleSubject.asObservable();

  public fileStatus = new FileUploadStatus();

  public users: User[];
  public user: User; //for the profile
  public refreshing: boolean;
  public selectedUser: User;
  public fileName: string;
  public profileImage: File;

  //constructor so we can initialize the object with default values of the user
  public editUser = new User();

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private userService: UserService,
    private notificationService: NotificationService
  ) {}

  ngOnInit(): void {
    this.user = this.authenticationService.getUserFromLocalCache();
    this.getUsers(true);
  }

  public changeTitle(title: string): void {
    //call the subject (titleSubject) and change value with next
    this.titleSubject.next(title);
  }

  public getUsers(showNotification: boolean): void {
    this.refreshing = true;
    // this.subscriptions.push(//replaced by subsink
    this.subs.add(
      this.userService.getUsers().subscribe(
        //if you have a subscrbtion => subscribe to it (like some one to receive the response)
        //2 possibilities: we have a response or an error response
        (response: User[]) => {
          this.userService.addUsersToLocalCache(response);
          this.users = response;
          this.refreshing = false;
          //if we have a notification
          if (showNotification) {
            this.sendNotification(
              NotificationType.SUCCESS,
              `${response.length} user(s) loaded successfully.`
            );
          }
        },
        //No response reveived, received error response
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(
            NotificationType.ERROR,
            errorResponse.error.message
          );
          this.refreshing = false;
        }
      )
    );
  }

  public onSelectUser(selectedUser: User): void {
    this.selectedUser = selectedUser;
    this.clickButton('openUserInfo');
  }

  public onProfileImageChange(fileName: string, profileImage: File): void {
    this.fileName = fileName;
    this.profileImage = profileImage;
  }

  public saveNewUser(): void {
    this.clickButton('new-user-save');
  }

  public onAddNewUser(userForm: NgForm): void {
    const formData = this.userService.createUserFormData(
      null,
      userForm.value,
      this.profileImage
    );
    //can (theoretically) take a while ==> subscription ==> subscribe
    // this.subscriptions.push(//replaced by subsink
    this.subs.add(
      this.userService.addUser(formData).subscribe(
        //if successfull => close modal
        (response: User) => {
          this.clickButton('new-user-close');
          this.getUsers(false); //no popup msg
          //clear profile image stuff
          this.fileName = null;
          this.profileImage = null;
          userForm.reset(); //clear form
          this.sendNotification(
            NotificationType.SUCCESS,
            `${response.firstName} ${response.lastName} added successfully`
          );
        },
        //there was an error
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(
            NotificationType.ERROR,
            errorResponse.error.message
          );
          this.profileImage = null;
        }
      )
    );
  }

  public onUpdateUser(): void {
    const formData = this.userService.createUserFormData(
      this.currentUserName,
      this.editUser,
      this.profileImage
    );
    // this.subscriptions.push(//replaced by subsink
    this.subs.add(
      this.userService.updateUser(formData).subscribe(
        (response: User) => {
          this.clickButton('closeEditUserModalButton');
          this.getUsers(false);
          this.fileName = null;
          this.profileImage = null;
          this.sendNotification(
            NotificationType.SUCCESS,
            `${response.firstName} ${response.lastName} updated successfully`
          );
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(
            NotificationType.ERROR,
            errorResponse.error.message
          );
          this.profileImage = null;
        }
      )
    );
  }

  public onUpdateCurrentUser(user: User): void {
    this.refreshing = true;
    this.currentUserName =
      this.authenticationService.getUserFromLocalCache().userName;
    const formData = this.userService.createUserFormData(
      this.currentUserName,
      user,
      this.profileImage
    );
    // this.subscriptions.push(//replaced by subsink
    this.subs.add(
      this.userService.updateUser(formData).subscribe(
        (response: User) => {
          //update user in the cache
          this.authenticationService.addUserToLocalCache(response);
          this.getUsers(false);
          this.fileName = null;
          this.profileImage = null;
          this.sendNotification(
            NotificationType.SUCCESS,
            `${response.firstName} ${response.lastName} updated successfully`
          );
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(
            NotificationType.ERROR,
            errorResponse.error.message
          );
          this.refreshing = false;
          this.profileImage = null;
        }
      )
    );
  }

  public onUpdateProfileImage(): void {
    const formData = new FormData();
    formData.append('userName', this.user.userName);
    formData.append('profileImage', this.profileImage);
    // this.subscriptions.push(//replaced by subsink
    this.subs.add(
      this.userService.updateUserProfileImage(formData).subscribe(
        (event: HttpEvent<any>) => {
          this.reportUploadProgress(event);
        },
        (errorResponse: HttpErrorResponse) => {
          this.sendNotification(
            NotificationType.ERROR,
            errorResponse.error.message
          );
          //if error => don't keep showing progress bar
          this.fileStatus.status = 'done';
        }
      )
    );
  }

  public updateProfileImage(): void {
    this.clickButton('profile-image-input');
  }

  public onLogOut(): void {
    //remove everything in the cache: the user, the token
    this.authenticationService.logout();

    //after logout, redirect user to login page
    this.router.navigate(['/login']);
    //display notification to user
    this.sendNotification(
      NotificationType.SUCCESS,
      `You've been successfully logged out`
    );
  }

  public onResetPassword(emailForm: NgForm): void {
    this.refreshing = true;
    const emailAddress = emailForm.value['reset-password-email'];
    // this.subscriptions.push(//replaced by subsink
    this.subs.add(
      this.userService.resetUserPassword(emailAddress).subscribe(
        (response: CustomHttpResponse) => {
          this.sendNotification(NotificationType.SUCCESS, response.message);
          this.refreshing = false;
        },
        (error: HttpErrorResponse) => {
          this.sendNotification(NotificationType.WARNING, error.error.message);
          this.refreshing = false;
        },
        () => emailForm.reset()
      )
    );
  }

  public onDeleteUser(userName: string): void {
    // this.subscriptions.push(//replaced by subsink
    this.subs.add(
      this.userService.deleteUser(userName).subscribe(
        (response: CustomHttpResponse) => {
          this.sendNotification(NotificationType.SUCCESS, response.message);
          this.getUsers(false);
        },
        (error: HttpErrorResponse) => {
          this.sendNotification(NotificationType.ERROR, error.error.message);
        }
      )
    );
  }

  public onEditUser(editUser: User): void {
    this.editUser = editUser;
    //with edit: if user changes it, and this is not done, it will fail in he backend
    //so before change ==> get a hold of it
    this.currentUserName = editUser.userName;

    this.clickButton('openUserEdit');
  }

  public searchUsers(searchTerm: string): void {
    const results: User[] = [];
    for (const user of this.userService.getUsersFromLocalCache()) {
      if (
        user.firstName.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1 ||
        user.lastName.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1 ||
        user.userName.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1 ||
        user.userId.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1 ||
        user.email.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1
      ) {
        results.push(user);
      }
      if (searchTerm.toLowerCase() === 'active' && user.active === true) {
        results.push(user);
      }
      if (searchTerm.toLowerCase() === 'inactive' && user.active === false) {
        results.push(user);
      }
    }
    this.users = results;
    if (results.length === 0 || !searchTerm) {
      this.users = this.userService.getUsersFromLocalCache();
    }
  }

  //to avoid memory leaks ==> unsubscribe subscriptions
  public ngOnDestroy(): void {
    // this.subscriptions.forEach((sub) => sub.unsubscribe());//replaced with subsink
    this.subs.unsubscribe();
  }

  //****************************************************************************
  //                  Getters and setters
  //****************************************************************************

  public get isAdmin(): boolean {
    return (
      this.getUserRole() === Role.ADMIN ||
      this.getUserRole() === Role.SUPER_ADMIN
    );
  }

  public get isManager(): boolean {
    return (
      this.isAdmin ||
      this.getUserRole() === Role.MANAGER ||
      this.getUserRole() === Role.HR
    );
  }

  public get isAdminOrManager(): boolean {
    return this.isAdmin || this.isManager;
  }

  //****************************************************************************
  //                  Helper methods
  //****************************************************************************

  private reportUploadProgress(event: HttpEvent<any>): void {
    switch (event.type) {
      case HttpEventType.UploadProgress:
        this.fileStatus.percentage = Math.round(
          (100 * event.loaded) / event.total
        );
        this.fileStatus.status = 'progress';
        break;
      case HttpEventType.Response:
        if (event.status === 200) {
          this.user.profileImageUrl = `${
            event.body.profileImageUrl
          }?time=${new Date().getTime()}`;
          this.sendNotification(
            NotificationType.SUCCESS,
            `${event.body.firstName}\'s profile image updated successfully`
          );
          this.fileStatus.status = 'done';
          break;
        } else {
          this.sendNotification(
            NotificationType.ERROR,
            `Unable to upload image. Please try again`
          );
          break;
        }
      default:
        `Finished all processes`;
    }
  }

  private getUserRole(): string {
    return this.authenticationService.getUserFromLocalCache().role;
  }

  private sendNotification(
    notificationType: NotificationType,
    message: string
  ): void {
    if (message) {
      this.notificationService.showNotification(notificationType, message);
    } else {
      this.notificationService.showNotification(
        notificationType,
        'An error occurred. Please try again.'
      );
    }
  }

  private clickButton(buttonId: string): void {
    document.getElementById(buttonId).click();
  }
}
