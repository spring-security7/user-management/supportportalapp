import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { HeaderType } from '../enums/header-type';
import { NotificationType } from '../enums/notification-type';
import { User } from '../models/user';
import { AuthenticationService } from '../services/authentication.service';
import { NotificationService } from '../services/notification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit, OnDestroy {
  public showLoading: boolean; //by default public - like to be specific
  private subscriptions: Subscription[] = [];

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private notificationService: NotificationService
  ) {}

  //default = public also for methods/functions
  public ngOnInit(): void {
    if (this.authenticationService.isUserLoggedIn()) {
      this.router.navigateByUrl('/user/management');
    } else {
      this.router.navigateByUrl('/login');
    }
  }

  public onLogin(user: User) {
    this.showLoading = true;

    this.subscriptions.push(
      //you have to subsribe to a subscription: waiting for the response
      this.authenticationService.login(user).subscribe(
        //if there is response => code in {} is executed
        //we need the whole response, not just the body (user) - but headers ==> token
        (response: HttpResponse<User>) => {
          const token = response.headers.get(HeaderType.JWT_TOKEN);
          //save token and user in local storage (cache)
          this.authenticationService.saveToken(token);
          this.authenticationService.addUserToLocalCache(response.body);

          //if all is ok ==> user to management page
          this.router.navigateByUrl('/user/management');

          //set loading to false
          this.showLoading = false;
        },
        //if there is an error ==> execute other code
        (errorResponse: HttpErrorResponse) => {
          this.sendErrorNotification(
            NotificationType.ERROR,
            errorResponse.error.message
          );
          this.showLoading = false;
        }
      )
    );
  }

  private sendErrorNotification(
    notificationType: NotificationType,
    message: string
  ): void {
    //check if message is not null, otherwhile send a default message
    if (message) {
      this.notificationService.showNotification(notificationType, message);
    } else {
      this.notificationService.showNotification(
        notificationType,
        'An error occured. Please try again.'
      );
    }
  }

  ngOnDestroy(): void {
    //to avoid memory leaks
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
