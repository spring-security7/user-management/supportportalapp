export class User {
  // public id: number;//pk

  public userId: string; //can be part of company policy when constructed
  public firstName: string;
  public lastName: string;
  public userName: string;
  // public password: string;
  public email: string;
  public profileImageUrl: string;
  public lastLoginDate: Date;
  public joinDate: Date;
  public lastLoginDateDisplay: Date;
  public role: string; //e.g. ROLE_USER {read, edit}; ROLE_ADMIN {delete}
  public authorities: [];
  public active: boolean;
  public notLocked: boolean;

  constructor() {
    this.userId = '';
    this.firstName = '';
    this.lastName = '';
    this.userName = '';
    this.email = '';
    this.profileImageUrl = '';
    this.lastLoginDate = null;
    this.lastLoginDateDisplay = null;
    this.joinDate = null;
    this.active = false;
    this.notLocked = false;
    this.role = '';
    this.authorities = [];
  }
}
