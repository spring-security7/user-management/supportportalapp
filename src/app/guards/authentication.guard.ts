import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { NotificationType } from '../enums/notification-type';
import { AuthenticationService } from '../services/authentication.service';
import { NotificationService } from '../services/notification.service';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationGuard implements CanActivate {

  constructor(private authenticationService: AuthenticationService, private router: Router,
    private notificationService:NotificationService) {

  }

  //NOT making a call to backend ==> else it would return an observable
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    return this.isUserLoggedIn();
  }

  private isUserLoggedIn(): boolean {

    if (this.authenticationService.isUserLoggedIn()) {
      return true;
    }

    this.router.navigate(['/login']);

    //send notification to user because not logged in
    this.notificationService.showNotification(NotificationType.ERROR,
      `You need to be logged in to see this page.`.toUpperCase());

    return false;
  }

}
